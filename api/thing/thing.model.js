'use strict';
var mongoose_1 = require('mongoose');
var ThingSchema = new mongoose_1["default"].Schema({
    name: String,
    info: String,
    active: Boolean
});
exports.__esModule = true;
exports["default"] = mongoose_1["default"].model('Thing', ThingSchema);
